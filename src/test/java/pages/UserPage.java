package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 04/08/2018.
 */
public class UserPage {
    WebDriver driver;

    @FindBy(xpath = "/html/body/div/div/button")
    WebElement saySomething_button;

    public UserPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkPage() {
        return driver.getTitle().contains("Logged In");
    }

    public void clicksaySomething() {
        saySomething_button.click();

    }

}

