package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 04/08/2018.
 */
public class LoginPage {

    WebDriver driver;

    @FindBy(id = "myid")
    WebElement username_field;

    @FindBy(id = "mypass")
    WebElement password_field;

    public LoginPage(WebDriver driver) {

        this.driver = driver;
        PageFactory.initElements(driver, this);

    }
    public boolean checkCorretPage() {
        return driver.getTitle().contains("Sign In");
    }


    public void login(String username, String password) {
        this.username_field.sendKeys(username);
        this.password_field.sendKeys(password);
        this.password_field.submit();
    }

    public void sendData(String username, String password) {
        this.username_field.sendKeys(username);
        this.password_field.sendKeys(password);

    }


    }

