package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 05/08/2018.
 */
public class Test02_SendToMultiElements extends BaseTest{

    @Test
    public void multiElements (){
        //Step 1 Click on Login
        welcomePage.clickLogin();
        //Step 2 Confirm you're on the Login page
        assertTrue(loginPage.checkCorretPage());
        //Step 3 Send the word test to Login fields
        loginPage.login("test","test");
    }
}
