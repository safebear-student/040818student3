package com.safebear.app;

import com.sun.org.apache.xerces.internal.impl.xpath.XPath;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 04/08/2018.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin () {
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkPage());
        //Step 2 click on the Login link and the Login Page loads
        welcomePage.clickLogin();
        //Step 3 confirm that we're now on the Login page
        assertTrue(loginPage.checkCorretPage());
        //Step 4 Login
        loginPage.login("testuser","testing");
        //Step 5 Assert we are on the Login page
        assertTrue(userPage.checkPage());
    }
}
